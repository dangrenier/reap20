class ApplicationMailer < ActionMailer::Base
  default from: 'PR+P USA <padgettportal@smallbizpros.com>'
  layout 'mailer'
end
